package main

import (
	"bufio"
	"flag"
	"fmt"
	"io"
	"log"
	"net"
	"strings"
)

func main() {
	port := flag.String("port", ":6688", "the port to listen")
	flag.Parse()
	if *port == "" {
		fmt.Println("port is empty")
		return
	}

	if !strings.HasPrefix(*port, ":") {
		*port = ":" + *port
	}

	log.Println("TCP Server Listen on: ", *port)
	lis, err := net.Listen("tcp4", *port)
	if err != nil {
		panic(err)
	}

	for {
		conn, err := lis.Accept()
		if err != nil {
			fmt.Println("accept error: ", err)
			continue
		}

		go handleConn(conn)
	}
}

func handleConn(conn net.Conn) {
	rw := bufio.NewReadWriter(bufio.NewReader(conn), bufio.NewWriter(conn))
	defer conn.Close()

	for {
		log.Println("Receive data...")
		msg, err := rw.ReadString('\n')
		if err == io.EOF {
			log.Println("Read ends")
			return
		}
		if err != nil {
			log.Println("Read msg error: ", err)
			return
		}

		log.Println("Received Message: ", msg)
		_, err = rw.WriteString("Client send: " + msg)
		if err != nil {
			log.Println("Write message back error: ", err)
		}
		err = rw.Flush()
		if err != nil {
			log.Println("Flush error: ", err)
		}
	}
}